import 'dart:async';

import 'package:news_app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewScreen2 extends StatefulWidget {
  String url;
  WebviewScreen2({this.url, Key key}) : super(key: key);

  @override
  _WebviewScreen2State createState() => _WebviewScreen2State();
}

class _WebviewScreen2State extends State<WebviewScreen2> {
  final _key = UniqueKey();
  num _stackToView = 1;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemesData.BACKGROUND_COLOR,
        title: Text(
          "News",
          style: ThemesData.setHeadingSize(),
        ),
      ),
      body: _body(),
    );
  }

  _body() {
    return (widget.url != null && widget.url.isNotEmpty)
        ? IndexedStack(
            index: _stackToView,
            children: [
              Builder(
                builder: (BuildContext context) {
                  return WebView(
                    initialUrl: widget.url,
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (WebViewController webViewController) {
                      _controller.complete(webViewController);
                    },
                    onPageStarted: _handleLoad,
                    gestureNavigationEnabled: true,
                  );
                },
              ),
              Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ],
          )
        : Container(
            alignment: Alignment.center,
            child: Text(
              "Invalid URL",
              style: ThemesData.setHeadingSize(),
            ),
          );
  }
}
