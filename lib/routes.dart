import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:news_app/news_screen.dart';
import 'package:news_app/splash_screen.dart';
import 'package:news_app/webview_screen-2.dart';
import 'package:news_app/webview_screen.dart';

Map<String, WidgetBuilder> routes = {
  '/': (BuildContext context) => Splash(),
};
Route<dynamic> getRoute(RouteSettings settings) {
  if (settings.name == "/news") {
    return _buildRoute(settings, NewScreen());
  } else if (settings.name == "/webview") {
    return _buildRoute(
      settings,
      WebviewScreen2(
        url: (settings != null && settings.arguments != null)
            ? settings.arguments as String
            : null,
      ),
    );
  } else {
    return null;
  }
}

MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
  return MaterialPageRoute(settings: settings, builder: (_) => builder);
}
