import 'dart:async';

import 'package:flutter/material.dart';
import 'package:news_app/theme.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemesData.width = MediaQuery.of(context).size.width;
    ThemesData.height = MediaQuery.of(context).size.height;
    ThemesData.setScreenRatio(ThemesData.height, ThemesData.width);

    Timer(Duration(seconds: 3), () {
      Navigator.pushNamedAndRemoveUntil(
          context, '/news', (Route<dynamic> route) => false);
    });

    // return Container(
    //   color: ThemesData.BACKGROUND_COLOR,
    //   // padding: EdgeInsets.all(50),
    //   child: Center(
    //     child: Text(
    //       'News App',
    //       style: TextStyle(color: Colors.white, fontSize: 24),
    //     ),
    //   ),
    // );
    return Scaffold(
      body: Container(
        color: ThemesData.BACKGROUND_COLOR,
        child: Center(
          child: Text(
            'News App',
            style: TextStyle(
              fontSize: 30,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
