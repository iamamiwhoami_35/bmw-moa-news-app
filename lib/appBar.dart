import 'package:news_app/theme.dart';
import 'package:flutter/material.dart';

Widget customAppBar(String title) {
  return AppBar(
    automaticallyImplyLeading: true,
    backgroundColor: Colors.transparent,
    elevation: 0,
    title: Text(
      title,
      style: ThemesData.appBarSize(),
    ),
  );
}
